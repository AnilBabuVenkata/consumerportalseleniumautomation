package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;

import util.ExcelFactory;
import util.RestCalls;
import util.utility;
//**************************************************************************************************************************************
public class MotorPage extends BasePageClass {
	public utility Util=new utility();
	private String uname=Util.randomIdentifier();
	String uniquename,email_id, OTP;

	public MotorPage(RemoteWebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	//**************************************************************************************************************************************
		public static final String amtreq_txtfield = "//input[@id='amountRequested']";
		public static final String continue_button = "//button[@id='nextButton']";
		public static final String deposit_txtfield = "//input[@id='deposit']";
		public static final String yearofcar_dropdown = "//select[@id='carFinanceYear']";
		public static final String carregis_txtfield = "//input[@id='carFinanceRegistration']";
		public static final String makeofcar_txtfield = "//input[@id='carFinanceMake']";
		public static final String modeofcar_txtfield = "//input[@id='carFinanceModel']";
		public static final String currkm_txtfield = "//input[@id='carFinanceCurrentKilometers']";
		public static final String financeamount_txtfield = "//input[@id='financeAmount']";
		public static final String outstanding_radiogroup = "//input[contains(@id,'haveOutstanding')]";
		//public static final String bankName_dropdown = "//select[@id='bankName']";
		public static final String bankName_dropdown = "//select[@id='bankName']";
		public static final String bankName_radiogroup = "//input[contains(@id,'bankName')]";
		public static final String yearsWithBank_txtfield ="//input[contains(@id,'yearsWithBank')]";
		public static final String yearsWithBank_radiogroup = "//select[@id='bankName']";
		public static final String letsgo_button = "//button[@id='letsGoBtn']";
		public static final String ppsNumber_txtfield = "//input[@id='officialReferenceNumber']";
		public static final String placeOfBirth_txtfield = "//input[@id='placeofBirth']";
		public static final String placeOfBirth_dropdown = "//select[@id='placeofBirth']";
		public static final String dateOfBirth_txtfield = "//input[@id='dateOfBirth']";
		public static final String paymentFrequency_radiogroup = "//input[contains(@id,'paymentFrequency')]";
		public static final String incomePayement_txtfield = "//input[@id='monthlyIncome']";
		public static final String progressmotorapp_sidebyarrow = "//a[contains(text(),'Appliant Details')]";
		public static final String persondetails_sidebyarrow = "//a[contains(text(),'Personal Details')]";
		public static final String addressdetails_sidebyarrow = "//a[contains(text(),'Address Details')]";
		public static final String employeedetails_sidebyarrow = "//a[contains(text(),'Employment Details')]";
		public static final String applicantdetails_link = "//div[@class='progress']//following::li[1]/a[1]";
		public static final String dependents_radiogroup ="//input[contains(@id,'haveDependents')]";
		public static final String dependents_dropdown ="//select[@id='noOfDependents']";
		public static final String appicantname_drill = "//div[@class='portal-application__wrap']";
		
	//**
	//**************************************************************************************************************************************
		
		public void fillMotorApplicationForm(RemoteWebDriver driver) throws Exception {
			ExcelFactory exclfactory = new ExcelFactory();
			exclfactory.ExcelFactory("F:\\FusionSoftTech\\Automation\\TestData\\ConsumerPortal.xls", "Motor");
			System.out.println(exclfactory.excelReadTextCell(1,0).toString());
			Thread.sleep(15000);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,0).toString(),amtreq_txtfield); //Reading Data from Amount Required
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,1).toString(),deposit_txtfield); //Reading Data from deposit
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			webActionHelperMethods.clickonradiobutton(outstanding_radiogroup,exclfactory.excelReadTextCell(1,7).toString());
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
		
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			webActionHelperMethods.selectdropdown(yearofcar_dropdown, exclfactory.excelReadTextCell(1,2).toString()); //Reading Data from year of car
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,3).toString(),carregis_txtfield);
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			//webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,4).toString(),makeofcar_txtfield);
			//webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			System.out.println(exclfactory.excelReadTextCell(1,5).toString());
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,5).toString(),modeofcar_txtfield);
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,6).toString(),currkm_txtfield);
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
		//	webActionHelperMethods.writeOnEditText(Integer.toString(4000),financeamount_txtfield);
			//webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			webActionHelperMethods.clickonradiobutton(CommonPage.hearabout_dropdown, exclfactory.excelReadTextCell(1,8).toString());
			//webActionHelperMethods.selectdropdown(CommonPage.hearabout_dropdown, "Google search");
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			webActionHelperMethods.ClickButtonAndWait(letsgo_button,10);
			webActionHelperMethods.writeOnEditText(""+uname+"@gmail.com",CommonPage.emailadd_txtbox);
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			webActionHelperMethods.selectdropdown(CommonPage.title_drpdwn,exclfactory.excelReadTextCell(1,9).toString());
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			webActionHelperMethods.writeOnEditText(uname,CommonPage.firstname_txtfield);
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,10).toString(),CommonPage.surname_txtfield);
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,11).toString(),CommonPage.mobile_txtfield);
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			webActionHelperMethods.clickonradiobutton(CommonPage.modeofcont_radiogroup, exclfactory.excelReadTextCell(1,12).toString());
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			webActionHelperMethods.selectdropdown(CommonPage.propcontacttime_drpdown,exclfactory.excelReadTextCell(1,13).toString());
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,14).toString(),CommonPage.password_txtbox);
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			//String otpurl="http://51.145.252.81/api/user/otp/"+""+uname+"@gmail.com";
			String otpurl="https://mytest.loanitt.com/api/user/otp/"+""+uname+"@gmail.com";
			//String otpurl="https://api.twilio.com/2010-04-01/Accounts/AC90f8c59d12ac5c803aa5be418ab8e25a/Messages.json?PageSize=1&Page=0";
			RestCalls restcalls=new RestCalls();
			String OTPVal=restcalls.getRequest(otpurl,"","","OTP");
			//OTP=restcalls.getRequest(otpurl,"AC90f8c59d12ac5c803aa5be418ab8e25a","2c4193fba4e25c84c07a41508d894fad","body");
			  System.out.println("---->"+OTPVal);
			Thread.sleep(3000);
			webActionHelperMethods.writeOnEditText(OTPVal,CommonPage.aotp_txtfield);
			WebElement element = driver.findElement(By.id("machineOTP"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);

		//	webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			Thread.sleep(3000);
			webActionHelperMethods.clickonradiobutton(CommonPage.martstatus_dropdown,exclfactory.excelReadTextCell(1,15).toString());
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,16).toString(),CommonPage.noofdepend_dropdown);
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			Thread.sleep(3000);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,17).toString(),CommonPage.streetaddress_txtbox);
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,18).toString(),CommonPage.homeaddtown_txtbox);
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			webActionHelperMethods.selectdropdown(CommonPage.county_dropdown,exclfactory.excelReadTextCell(1,19).toString());
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,20).toString(),CommonPage.homeaddresspostcode_txtbox);
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			webActionHelperMethods.clickonradiobutton(CommonPage.residentialstatus_radiogroup,exclfactory.excelReadTextCell(1,21).toString());
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			//webActionHelperMethods.selectdropdown(CommonPage.yrsatcurradd_dropdown,exclfactory.excelReadTextCell(1,22).toString());
			webActionHelperMethods.clickonradiobutton(CommonPage.yrsatcurradd_radiogroup,exclfactory.excelReadTextCell(1,22).toString());
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,23).toString(),CommonPage.rentaggment_txtfield);
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			webActionHelperMethods.clickonradiobutton(bankName_radiogroup,exclfactory.excelReadTextCell(1,24).toString());
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			webActionHelperMethods.clickonradiobutton(yearsWithBank_txtfield,exclfactory.excelReadTextCell(1,25).toString());
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,26).toString(),ppsNumber_txtfield);
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			//webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,27).toString(),placeOfBirth_txtfield);
			webActionHelperMethods.selectdropdown(placeOfBirth_dropdown,exclfactory.excelReadTextCell(1,27).toString());
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,28).toString(),dateOfBirth_txtfield);
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			Thread.sleep(3000);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,29).toString(),Mortgageswitchpage.empname_txtfield );
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,30).toString(),Mortgageswitchpage.empaddstreet_txtfield);
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,31).toString(),Mortgageswitchpage.emptown_txtfield);
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			webActionHelperMethods.selectdropdown(Mortgageswitchpage.empcounty_drpdown,exclfactory.excelReadTextCell(1,32).toString());
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			//webActionHelperMethods.selectdropdown(Mortgageswitchpage.empname_txtfield, "Full Time");
			webActionHelperMethods.clickonradiobutton(Mortgageswitchpage.empstatus_radiogroup,exclfactory.excelReadTextCell(1,33).toString());
			//empstatus_dropdown
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			webActionHelperMethods.clickonradiobutton(Mortgageswitchpage.yrswithemp_radiogroup, exclfactory.excelReadTextCell(1,34).toString());
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			webActionHelperMethods.clickonradiobutton(paymentFrequency_radiogroup,exclfactory.excelReadTextCell(1,35).toString());
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,36).toString(),incomePayement_txtfield);
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,10);
			exclfactory.finalize();
			
			
			
		
		}
		
	//**************************************************************************************************************************************
		public void clickOnApplicantDetials(RemoteWebDriver driver) throws Exception {
			Thread.sleep(10000);
			JavascriptExecutor jse= (JavascriptExecutor)driver;
			WebElement progressmotorapp = driver.findElement(By.xpath(progressmotorapp_sidebyarrow));
			jse.executeScript("arguments[0].click();", progressmotorapp);
			
			
			//webActionHelperMethods.ClickButtonAndWait(progressmotorapp_sidebyarrow,10);
			
			//webActionHelperMethods.clickbutton(applicantdetails_link);
			//webActionHelperMethods.clickbutton(appicantname_drill);
			//webActionHelperMethods.clickbutton(applicantdetails_link);
			Thread.sleep(5000);
			WebElement personadetials = driver.findElement(By.xpath(persondetails_sidebyarrow));
			jse.executeScript("arguments[0].click();", personadetials);
			Thread.sleep(10000);
			//webActionHelperMethods.ClickButtonAndWait(persondetails_sidebyarrow,10);
		}
	//**************************************************************************************************************************************
	    public void clickOnAddressDetails() throws Exception {
	    	Thread.sleep(3000);
	    	webActionHelperMethods.ClickButtonAndWait(addressdetails_sidebyarrow,10);
	    }
	//**************************************************************************************************************************************
	public String getMartialStatus() {
		return webActionHelperMethods.getobjectValue(CommonPage.martstatus_dropdown);
	}
	//**************************************************************************************************************************************
	public void clickOnUpdateButton() throws Exception {
		webActionHelperMethods.ClickButtonAndWait(CommonPage.update_button,10);
	}

	//**************************************************************************************************************************************
	public void clickOnNextButton() throws Exception {
		webActionHelperMethods.ClickButtonAndWait(continue_button,10);
	}
	//**************************************************************************************************************************************	
	public String getdependentsListValue() {
		return webActionHelperMethods.getSelectedDropDownValue(dependents_dropdown);
	}
	//**************************************************************************************************************************************
	public String getMortageRentvalue() {
		return webActionHelperMethods.getobjectValue(CommonPage.rentaggment_txtfield);
	}
	//**************************************************************************************************************************************
	public String getBankvalue() {
		return webActionHelperMethods.getobjectValue(bankName_radiogroup);
	}	
	//**************************************************************************************************************************************
	public String getYearswithBankvalue(String val) {
		return webActionHelperMethods.getRadioObjectValue(yearsWithBank_txtfield,val);
	}
	//**************************************************************************************************************************************
	public String getPPSValue() {
		System.out.println(webActionHelperMethods.getobjectValue(ppsNumber_txtfield));
		return webActionHelperMethods.getobjectValue(ppsNumber_txtfield);
	}	
	//**************************************************************************************************************************************
	//**************************************************************************************************************************************
	public String getPlaceofBirthValue() {
		return webActionHelperMethods.getSelectedDropDownValue(placeOfBirth_dropdown);
	}	
	//**************************************************************************************************************************************
	public String getDateofBirthValue() {
		return webActionHelperMethods.getobjectValue(dateOfBirth_txtfield);
	}	
	//**************************************************************************************************************************************
	public String getPaymentFreqValue(String val) {
		return webActionHelperMethods.getRadioObjectValue(paymentFrequency_radiogroup,val);
	}	
	//**************************************************************************************************************************************
	public String getIncomePayValue() {
		return webActionHelperMethods.getobjectValue(incomePayement_txtfield);
	}
	//**************************************************************************************************************************************
	public String getStreetAdressValue() {
		return webActionHelperMethods.getobjectValue(CommonPage.streetaddress_txtbox);
	}
	//**************************************************************************************************************************************
	public String getHomeTownValue() {
		return webActionHelperMethods.getobjectValue(CommonPage.homeaddtown_txtbox);
	}
	//**************************************************************************************************************************************

	public String getCountryDrpDownValue() {
		return webActionHelperMethods.getSelectedDropDownValue(CommonPage.county_dropdown);
	}
	//**************************************************************************************************************************************
	public String getHomeAddressPostCodeValue() {
		return webActionHelperMethods.getobjectValue(CommonPage.homeaddresspostcode_txtbox);
	}
	//**************************************************************************************************************************************
	public String getResidentialStatusValue(String val) {
		return webActionHelperMethods.getRadioObjectValue(CommonPage.residentialstatus_radiogroup,val);
	}	
	//**************************************************************************************************************************************
	public String getYrsatCurrAddValue(String val) {
		return webActionHelperMethods.getRadioObjectValue(CommonPage.yrsatcurradd_radiogroup,val);
	}	
	//**************************************************************************************************************************************
	public String getEmpNameValue() {
		return webActionHelperMethods.getobjectValue(Mortgageswitchpage.empname_txtfield);
	}
	//**************************************************************************************************************************************
	public String getEmpStreetValue() {
		return webActionHelperMethods.getobjectValue(Mortgageswitchpage.empaddstreet_txtfield);
	}

	//**************************************************************************************************************************************
	public String getEmpTownValue() {
		return webActionHelperMethods.getobjectValue(Mortgageswitchpage.emptown_txtfield);
	}
	//**************************************************************************************************************************************

	public String getEmpCountryDrpDownValue() {
		return webActionHelperMethods.getSelectedDropDownValue(Mortgageswitchpage.empcounty_drpdown);
	}
	//**************************************************************************************************************************************
	public String getEmpStatusValue(String val) {
		return webActionHelperMethods.getRadioObjectValue(Mortgageswitchpage.empstatus_radiogroup,val);
	}	
	//**************************************************************************************************************************************
	public String getYrswithEmpValue(String val) {
		return webActionHelperMethods.getRadioObjectValue(Mortgageswitchpage.yrswithemp_radiogroup,val);
	}	
	//**************************************************************************************************************************************
	public void clickOnEmpDetailsSection() throws Exception {
		Thread.sleep(3000);
		webActionHelperMethods.ClickButtonAndWait(employeedetails_sidebyarrow,10);
	}
	//**************************************************************************************************************************************
	}
