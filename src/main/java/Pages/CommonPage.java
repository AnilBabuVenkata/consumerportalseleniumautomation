package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;

public class CommonPage extends BasePageClass {
	//**************************************************************************************************************************************
	public CommonPage(RemoteWebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
//*******************************************************************************************************************************************
	
	public static final String continue_button = "//button[@id='nextButton']";
 	//public static final String hearabout_dropdown = "//select[@id='hearAbout']";
	public static final String hearabout_dropdown = "//input[@name='hearAbout']";
    public static final String emailadd_txtbox =  "//input[@id='emailAddress']";
    public static final String aemailadd_txtbox =  "//input[@id='emailAdd']";
    public static final String password_txtbox = "//input[@id='passwordS']";
	public static final String title_drpdwn = "//select[@id='title']";
	public static final String firstname_txtfield = "//input[@id='firstName']";
	public static final String afirstname_txtfield = "//input[@id='afirstName']";
	public static final String surname_txtfield = "//input[@id='surname']";
	public static final String asurname_txtfield = "//input[@id='asurname']";
	public static final String mobile_txtfield = "//input[@id='mobile']";
	public static final String modeofcont_radiogroup = "//input[contains(@id,'methodOfContact')]";
	public static final String propcontacttime_drpdown = "//select[@id='proposedContactTime']";
	public static final String dob_txtfield = "//input[@id='dateOfBirth']";
	public static final String maidenname_txtfield = "//input[@id='amaidenname']";
	public static final String martstatus_dropdown = "//input[contains(@id,'maritalStatus')]";
	public static final String noofdepend_dropdown = "//select[@id='noOfDependents']";
	public static final String streetaddress_txtbox = "//input[@id='homeAddressStreet']";
	public static final String homeaddtown_txtbox = "//input[@id='homeAddressTown']";
	public static final String county_dropdown = "//select[@id='homeAddressCounty']";
	public static final String homeaddresspostcode_txtbox = "//input[@id='homeAddressPostCode']";
	public static final String residentialstatus_radiogroup = "//input[contains(@id,'residentialStatus')]";
	public static final String yrsatcurradd_dropdown = "//select[@id='yearsAtCurrentAddress']";
	public static final String yrsatcurradd_radiogroup = "//input[@name='yearsAtCurrentAddress']";
	public static final String ppsn_txtfield = "//input[@id='ppsn']";
	public static final String nationality_txtfield = "//input[@id='nationality']";
	public static final String annincome_txtfield = "//input[@id='annualIncome']";
	public static final String aotp_txtfield = "//input[@id='otpS']";
	public static final String otp_txtfield = "//input[@id='otp']";
	public static final String macotp_button = "//p[@id='machineOTP']";
	public static final String rentaggment_txtfield = "//input[@id='monthlyRentOrMortgage']";
	public static final String update_button = "//button[@id='updateButton']";
	
	
	
	
	
}
