package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;

public class DashboardPage extends BasePageClass {
//********************************************************************************************************************************
	public DashboardPage(RemoteWebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
//********************************************************************************************************************************
	@FindBy(xpath="//button[@id='myApp']")
	WebElement myApplicationstab;
	
	@FindBy(xpath="//button[@id='newApp']")
	WebElement newApplicationstab;
//********************************************************************************************************************************
	public void clickOnMyApptab() {
		myApplicationstab.click();
	}
//********************************************************************************************************************************
	public void clickOnNewApptab() {
		newApplicationstab.click();
	}
	
//********************************************************************************************************************************	
}
