package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;

import actionHelper.WebActionHelperMethods;

public class SignInPage extends BasePageClass{
//***********************************************************************************************************************
	public SignInPage(RemoteWebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
//***********************************************************************************************************************	
	@FindBy(xpath="//a[@id='link-signup']")
	WebElement signUp_link;
	
	@FindBy(xpath="//input[@id='loginEmail']")
	WebElement email_txtbox;
	
	@FindBy(xpath="//input[@id='loginPassword']")
	WebElement pwd_txtbox;
	
	@FindBy(xpath="//button[@id='btn-login']")
	WebElement login_button;
	
	@FindBy(xpath="//input[@id='otp']")
	WebElement otp_txtbox;
	
	@FindBy(xpath="//button[@id='btn-signin-otp']")
	WebElement otpsignIn_button;
//***********************************************************************************************************************
	public void signIn(String uname) {
		email_txtbox.sendKeys(uname);
		pwd_txtbox.sendKeys("Welcome@123");
		login_button.click();
	}

//***********************************************************************************************************************	

  public void clickOnSignUp() {
	  signUp_link.click();
  }
 //*********************************************************************************************************************** 
  public void signInWithOTP(String otpno) {
	  otp_txtbox.sendKeys(otpno);
	  otpsignIn_button.click();
  }
  
//***********************************************************************************************************************
  
}
