package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;

import util.RestCalls;
import util.utility;

public class MortgagePage extends BasePageClass{
	public utility Util=new utility();
	private String uname=Util.randomIdentifier();
	String uniquename;
	public MortgagePage(RemoteWebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
//**************************************************************************************************************************************************	
	public static final String appliant_drilldown = "/html/body/div[1]/div/div/div/div[4]/div[2]/div[2]/div/div/div/div[2]/div[2]/div[3]/i";
	public static final String buyer_dropdown = "//select[@id='typeOfBuyer']";
	public static final String amtreq_txtbox = "//input[@id='amountRequested']";
	public static final String finamount_txtbox = "//input[@id='financeAmount']";
	public static final String repayment_dropdown = "//select[@id='repaymentTerm']";
	public static final String joinsingleapp_radiogroup = "//input[contains(@id,'appType')]";
//**************************************************************************************************************************************************
	public void fillinmortgageforms() throws Exception {
		webActionHelperMethods.selectdropdown(buyer_dropdown, "First Time Buyer");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText(Integer.toString(5000), amtreq_txtbox);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText(Integer.toString(2000), finamount_txtbox);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.selectdropdown(repayment_dropdown, "5 - 35 Years");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.clickonradiobutton(joinsingleapp_radiogroup, "Single");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.selectdropdown(CommonPage.hearabout_dropdown, "Google search");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText(""+uname+"@gmail.com",CommonPage.emailadd_txtbox);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.selectdropdown(CommonPage.title_drpdwn, "Mr");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText(uname,CommonPage.firstname_txtfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("T",CommonPage.surname_txtfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		//webActionHelperMethods.writeOnEditText("919885192249",CommonPage.mobile_txtfield);
		webActionHelperMethods.writeOnEditText("+1 888-889-9999",CommonPage.mobile_txtfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.clickonradiobutton(CommonPage.modeofcont_radiogroup, "email");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.selectdropdown(CommonPage.propcontacttime_drpdown, "Morning");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("admin@123",CommonPage.password_txtbox);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
	//	String otpurl="http://51.145.252.81/api/user/otp/"+""+uname+"@gmail.com";
		//RestCalls restcalls=new RestCalls();
		//String OTPVal=restcalls.getRequest(otpurl,"","","OTP");
		//driver.findElement(CommonPage.otp_txtfield)
		Thread.sleep(135000);
		//webActionHelperMethods.writeOnEditText(OTPVal,CommonPage.aotp_txtfield);
		webActionHelperMethods.writeOnEditText("123456",CommonPage.aotp_txtfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.selectdropdown(CommonPage.title_drpdwn, "Mr");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("anil",CommonPage.afirstname_txtfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("T",CommonPage.asurname_txtfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("anil.test@gmail.com",CommonPage.aemailadd_txtbox);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("11/11/1990",CommonPage.dob_txtfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("T",CommonPage.maidenname_txtfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.clickonradiobutton(CommonPage.martstatus_dropdown, "Single");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("0",CommonPage.noofdepend_dropdown);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("Test Street",CommonPage.streetaddress_txtbox);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("Test Town",CommonPage.homeaddtown_txtbox);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.selectdropdown(CommonPage.county_dropdown, "Ireland");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("9001",CommonPage.homeaddresspostcode_txtbox);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.clickonradiobutton(CommonPage.residentialstatus_radiogroup, "Home Owner");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.selectdropdown(CommonPage.yrsatcurradd_dropdown, "2");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("9001",CommonPage.ppsn_txtfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("INDIAN",CommonPage.nationality_txtfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("15000",CommonPage.annincome_txtfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		
webActionHelperMethods.clickonradiobutton(Mortgageswitchpage.varincome_radiogroup, "Joint");
		
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("Google",Mortgageswitchpage.empname_txtfield );
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("Google Street",Mortgageswitchpage.empaddstreet_txtfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("Google Town",Mortgageswitchpage.emptown_txtfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.selectdropdown(Mortgageswitchpage.empcounty_drpdown, "Ireland");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("Software engineer",Mortgageswitchpage.empoccp_txtfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.selectdropdown(Mortgageswitchpage.empstatus_dropdown, "Full Time");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.clickonradiobutton(Mortgageswitchpage.yrswithemp_radiogroup, "2");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("Tesla",Mortgageswitchpage.prevemp_txtfield );
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("Tesla Street",Mortgageswitchpage.prevempaddstreet_txtfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("Tesla Town",Mortgageswitchpage.prevempaddtown_txtfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.selectdropdown(Mortgageswitchpage.prevempaddcounty_dropdown, "Ireland");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("Software engineer",Mortgageswitchpage.prevempoccp_txtfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.selectdropdown(Mortgageswitchpage.selprevempcontract_drpdown, "Full Time");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.clickonradiobutton(Mortgageswitchpage.yrswithprevemp_radiogroup, "2");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.clickonradiobutton(Mortgageswitchpage.jointapp_radiogroup, "no");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		

		
		
	}
}
