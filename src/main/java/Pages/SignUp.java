package Pages;

import java.util.UUID;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;

import util.utility;

public class SignUp extends BasePageClass {
	public utility Util=new utility();
	private String uname=Util.randomIdentifier();
	String uniquename,email_id;
//******************************************************************************************************************************
	public SignUp(RemoteWebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
//******************************************************************************************************************************	
	@FindBy(xpath="//input[@id='signupName']")
	WebElement name_TxtBox;
	
	@FindBy(xpath="//input[@id='signupEmail']")
	WebElement email_TxtBox;
	
	@FindBy(xpath="//input[@id='signupMobile']")
	WebElement mobile_TxtBox;
	
	@FindBy(xpath="//input[@id='signupPassword']")
	WebElement password_TxtBox;
	
	@FindBy(xpath="//input[@id='signupRepeatPassword']")
	WebElement repeatpassword_TxtBox;
	
	@FindBy(xpath="//button[@id='btn-signup']")
	WebElement signUp_Button;
	
	@FindBy(xpath="//h1[contains(text(),'Sign Up')]")
	WebElement signUp_Header;
//******************************************************************************************************************************	
	public void Signup() {
		name_TxtBox.sendKeys(uname);
		 uniquename=name_TxtBox.getAttribute("value");
		email_TxtBox.sendKeys(""+uname+"@gmail.com");
		email_id=email_TxtBox.getAttribute("value");
		mobile_TxtBox.sendKeys("+447480803726");
		password_TxtBox.sendKeys("Anil@123");
		repeatpassword_TxtBox.sendKeys("Anil@123");
		signUp_Button.click();
	}
//******************************************************************************************************************************
	public String getTextValue() {
		return uniquename;
		
	}
	
//******************************************************************************************************************************
		public String getEmailIdValue() {
			return email_id;
			
		}
	
//******************************************************************************************************************************	
   public String getHeader() {
	return signUp_Header.getText();
	   
   }

//******************************************************************************************************************************
}
