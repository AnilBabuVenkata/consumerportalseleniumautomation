package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import java.util.UUID;

import util.RestCalls;
import util.utility;

public class Mortgageswitchpage extends BasePageClass{
	public utility Util=new utility();
	private String uname=Util.randomIdentifier();
	String uniquename,email_id, OTP;
//**************************************************************************************************************************************************************
	public Mortgageswitchpage(RemoteWebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
//**************************************************************************************************************************************************************
	
	public static final String outstandingbal_textfield = "//input[@id='existingMortgageOutstandingBalance']";
	public static final String currintrate_textfield = "//input[@id='existingMortgageInterestRate']";
	public static final String currmonrepay_textfield = "//input[@id='existingMortgageMonthlyRepayment']";
	public static final String remyrcurrmort_dropdown = "//select[@id='repaymentTerm']";
	public static final String estpropval_textfield = "//input[@id='amountRequested']";
	public static final String topupcheck_radiogroup = "//input[contains(@id,'topupCheck')]";
	public static final String varincome_radiogroup = "//input[contains(@id,'variableIncomeCheck')]";
	public static final String singjoint_radiogroup = "//input[contains(@id,'appType')]";
	public static final String empname_txtfield = "//input[@id='employerName']";
	public static final String empaddstreet_txtfield = "//input[@id='employerAddressStreet']";
	public static final String emptown_txtfield = "//input[@id='employerAddressTown']";
	public static final String empcounty_drpdown = "//select[@id='employerAddressCounty']";
	public static final String empoccp_txtfield = "//input[@id='employerOccupation']";
	public static final String empstatus_dropdown = "//select[@id='employmentContract']";
	public static final String empstatus_radiogroup = "//input[@name='employmentContract']";
	public static final String yrswithemp_radiogroup = "//input[contains(@id,'employerYearsWith')]";
	public static final String prevemp_txtfield = "//input[@id='prevEmployerName']";
	public static final String prevempaddstreet_txtfield = "//input[@id='prevEmployerAddressStreet']";
	public static final String prevempaddtown_txtfield = "//input[@id='prevEmployerAddressTown']";
	public static final String prevempaddcounty_dropdown = "//select[@id='prevEmployerAddressCounty']";
	public static final String prevempoccp_txtfield = "//input[@id='prevEmployerOccupation']";
	public static final String selprevempcontract_drpdown = "//select[@id='prevEmploymentContract']";
	public static final String yrswithprevemp_radiogroup = "//input[contains(@id,'prevEmployerYearsWith')]";
	public static final String jointapp_radiogroup = "//input[contains(@id,'haveJointApplicant')]";
	
	//**************************************************************************************************************************************************************
	public void fillApplicantDetails() throws Exception {
		webActionHelperMethods.writeOnEditText(Integer.toString(2000), outstandingbal_textfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		Thread.sleep(10000);
		webActionHelperMethods.writeOnEditText(Integer.toString(5), currintrate_textfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText(Integer.toString(200), currmonrepay_textfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.selectdropdown(remyrcurrmort_dropdown, "5 - 35 Years");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText(Integer.toString(7000), estpropval_textfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.clickonradiobutton(topupcheck_radiogroup, "No");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.clickonradiobutton(singjoint_radiogroup, "Single");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
	    webActionHelperMethods.selectdropdown(CommonPage.hearabout_dropdown, "Google search");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText(""+uname+"@gmail.com",CommonPage.emailadd_txtbox);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.selectdropdown(CommonPage.title_drpdwn, "Mr");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText(uname,CommonPage.firstname_txtfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("T",CommonPage.surname_txtfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		//+1 888-889-9999
		//webActionHelperMethods.writeOnEditText("919885192249",CommonPage.mobile_txtfield);
		webActionHelperMethods.writeOnEditText("+1 888-889-9999",CommonPage.mobile_txtfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.clickonradiobutton(CommonPage.modeofcont_radiogroup, "email");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.selectdropdown(CommonPage.propcontacttime_drpdown, "Morning");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		Thread.sleep(2000);
		webActionHelperMethods.writeOnEditText("admin@123",CommonPage.password_txtbox);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		Thread.sleep(10000);
		String otpurl="http://51.145.252.81/api/user/otp/"+""+uname+"@gmail.com";
		RestCalls restcalls=new RestCalls();
		String OTPVal=restcalls.getRequest(otpurl,"","","OTP");
		//driver.findElement(CommonPage.otp_txtfield)
		Thread.sleep(3000);
		webActionHelperMethods.writeOnEditText(OTPVal,CommonPage.aotp_txtfield);
		//webActionHelperMethods.writeOnEditText("123456",CommonPage.aotp_txtfield);
	//	webActionHelperMethods.clickbutton(CommonPage.continue_button);
		WebElement element = driver.findElement(By.id("machineOTP"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);

		webActionHelperMethods.selectdropdown(CommonPage.title_drpdwn, "Mr");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("anil",CommonPage.afirstname_txtfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("T",CommonPage.asurname_txtfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("anil.test@gmail.com",CommonPage.aemailadd_txtbox);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("11/11/1990",CommonPage.dob_txtfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("T",CommonPage.maidenname_txtfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.clickonradiobutton(CommonPage.martstatus_dropdown, "Single");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("0",CommonPage.noofdepend_dropdown);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("Test Street",CommonPage.streetaddress_txtbox);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("Test Town",CommonPage.homeaddtown_txtbox);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.selectdropdown(CommonPage.county_dropdown, "Ireland");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("9001",CommonPage.homeaddresspostcode_txtbox);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.clickonradiobutton(CommonPage.residentialstatus_radiogroup, "Home Owner");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.selectdropdown(CommonPage.yrsatcurradd_dropdown, "2");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("9001",CommonPage.ppsn_txtfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("INDIAN",CommonPage.nationality_txtfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("15000",CommonPage.annincome_txtfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.clickonradiobutton(varincome_radiogroup, "Joint");
		
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("Google",empname_txtfield );
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("Google Street",empaddstreet_txtfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("Google Town",emptown_txtfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.selectdropdown(empcounty_drpdown, "Ireland");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("Software engineer",empoccp_txtfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.selectdropdown(empstatus_dropdown, "Full Time");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.clickonradiobutton(yrswithemp_radiogroup, "2");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("Tesla",prevemp_txtfield );
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("Tesla Street",prevempaddstreet_txtfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("Tesla Town",prevempaddtown_txtfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.selectdropdown(prevempaddcounty_dropdown, "Ireland");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.writeOnEditText("Software engineer",prevempoccp_txtfield);
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.selectdropdown(selprevempcontract_drpdown, "Full Time");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.clickonradiobutton(yrswithprevemp_radiogroup, "2");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		webActionHelperMethods.clickonradiobutton(jointapp_radiogroup, "no");
		webActionHelperMethods.clickbutton(CommonPage.continue_button);
		
		
		
		
		
		
		
		
		
	}
//**************************************************************************************************************************************************************	
	
}
