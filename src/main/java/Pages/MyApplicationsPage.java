package Pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class MyApplicationsPage extends BasePageClass {
//**************************************************************************************************************************************************************
	public MyApplicationsPage(RemoteWebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
//**************************************************************************************************************************************************************
	@FindBy(xpath="//div[@id='allApplications']")
	WebElement tableMyApplications;
	
	@FindBy(xpath="//div[@id='allApplications']/div[2]/div[1]/div[3]/i[1]")
	WebElement mortgageDrillDown;
	
	@FindBy(xpath="/html/body/div[1]/div/div/div/div[4]/div[2]/div[2]/div/div/div/div[2]/div[2]/div[3]/i")
	WebElement appliantDrilldown;
	
	@FindBy(xpath="/html/body/div[1]/div/div/div/div[4]/div[2]/div[2]/div/div/div/div[2]/div[4]/div[3]/i")
	WebElement fileUploadDrilldown;
	
	@FindBy(xpath="//div[@id='allApplicants']/div[1]/div[1]/div[1]/a")
	WebElement mortapplicantName;
	
	
	
	@FindBy(xpath="//div[2]/div[1]/div/div[2]/div/div/div/div/div[1]/div[1]")
	WebElement genderTxt;
	
	@FindBy(xpath="//div[1]/div/div[2]/div/div/div/div/div[1]/div[2]")
	WebElement martStatusTxt;
	
	@FindBy(xpath="//div[1]/div/div[2]/div/div/div/div/div[2]/div[1]")
	WebElement mobileTxt;
	
	@FindBy(xpath="//div[1]/div/div[2]/div/div/div/div/div[2]/div[2]")
	WebElement emailTxt;
	
	@FindBy(xpath="//div[1]/div/div[2]/div/div/div/div/div[3]/div[1]")
	WebElement dobTxt;
	
	@FindBy(xpath="//div[1]/div/div[2]/div/div/div/div/div[3]/div[2]")
	WebElement placeofbirthTxt;
	
	@FindBy(xpath="//div[1]/div/div[2]/div/div/div/div/div[4]/div[2]/p")
	WebElement nextBtn;
	
	@FindBy(xpath="//div[1]/div/div[2]/div/div/div/div/div[5]/div/div/a[1]")
	WebElement personadetailsBtn;
	
	public static final String genderTxt_XPATH = "//div[2]/div[1]/div/div[2]/div/div/div/div/div[1]/div[1]";
	public static final String martStatusTxt_XPATH = "//div[1]/div/div[2]/div/div/div/div/div[1]/div[2]";
	public static final String mobileTxt_XPATH = "//div[1]/div/div[2]/div/div/div/div/div[2]/div[1]";
	public static final String emailTxt_XPATH = "//div[1]/div/div[2]/div/div/div/div/div[2]/div[2]";
	public static final String dobTxt_XPATH = "//div[1]/div/div[2]/div/div/div/div/div[3]/div[1]";
	public static final String placeofbirthTxt_XPATH = "//div[1]/div/div[2]/div/div/div/div/div[2]/div[2]";
	public static final String nextBtn_XPATH = "//div[1]/div/div[2]/div/div/div/div/div[4]/div[2]/p";
	public static final String personadetailsBtn_XPATH = "//div[1]/div/div[2]/div/div/div/div/div[5]/div/div/a[1]";
	public static final String updateBtn_XPATH = "//button[@id='updateButton']";
	public static final String monthlyRent_XPATH = "//input[@id='monthlyRentOrMortgage']";
	public static final String bankName_XPATH = "//select[@id='bankName']";
	public static final String yearsWithBank_XPATH ="//input[contains(@id,'yearsWithBank')]";
	public static final String ppsNumber_XPATH = "//input[@id='officialReferenceNumber']";
	public static final String placeOfBirth_XPATH = "//input[@id='placeofBirth']";
	public static final String dateOfBirth_XPATH = "//input[@id='dateOfBirth']";
	public static final String paymentFrequency_XPATH = "//input[contains(@id,'paymentFrequencyMonthly')]";
	public static final String incomePayement_XPATH = "//input[@id='monthlyIncome']";
//**************************************************************************************************************************************************************
	public void onClick() {
		List<WebElement> elements=driver.findElements(By.xpath("//div[@id='allApplications']"));
		System.out.println(elements.size());
		for(int p=0;p<elements.size();p++) {
			elements.get(p).click();
		}
	}
	
//**************************************************************************************************************************************************************	
	public String getGenderTxt() {
		return webActionHelperMethods.getObjectText(genderTxt_XPATH).split("\n")[1];
	}
//**************************************************************************************************************************************************************
	public String getMaritalStatusTxt() {
		return webActionHelperMethods.getObjectText(martStatusTxt_XPATH).split("\n")[1];
	}
//**************************************************************************************************************************************************************
	public String getMobileTxt() {
		return webActionHelperMethods.getObjectText(mobileTxt_XPATH).split("\n")[1];
	}
//**************************************************************************************************************************************************************
	public String getEmailTxt() {
		return webActionHelperMethods.getObjectText(emailTxt_XPATH).split("\n")[1];
	}
//**************************************************************************************************************************************************************	
	public String getDOBTxt() {
		return webActionHelperMethods.getObjectText(dobTxt_XPATH).split("\n")[1];
	}
//**************************************************************************************************************************************************************
	public String getPlaceofBirthTxt() {
		return webActionHelperMethods.getObjectText(placeofbirthTxt_XPATH).split("\n")[1];
	}
//**************************************************************************************************************************************************************
	public void clickOnNextBtn() {
		 try {
			webActionHelperMethods.ClickButtonAndWait(nextBtn_XPATH,10);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
//**************************************************************************************************************************************************************	
	public void clickOnPersonalDetailsBtn() {
		 try {
			webActionHelperMethods.ClickButtonAndWait(personadetailsBtn_XPATH,10);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
//**************************************************************************************************************************************************************
	public void clickOnAppliant() {
		appliantDrilldown.click();
		mortapplicantName.click();
		//String gendervalue=genderTxt.getText().split("\n")[1];
		//System.out.println(genderTxt);
		
	}
//**************************************************************************************************************************************************************	
	
	public void clickOnUpdateBtn() {
		 try {
			webActionHelperMethods.ClickButtonAndWait(updateBtn_XPATH,10);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
//**************************************************************************************************************************************************************
	public String getRadioButtonProperty() {
		List<WebElement> radio_options = driver.findElements(By.xpath("//input[contains(@id,'maritalStatus')]"));
		String getVal = null;
	    for(WebElement option:radio_options)
	        {
	            if(option.isSelected())
	            {
	              getVal=option.getAttribute("value");
	             break;
	            }
	        }
		return getVal;
	}
//**************************************************************************************************************************************************************	
	public String getDependentsRadioButtonProperty() {
		List<WebElement> radio_options = driver.findElements(By.xpath("//input[contains(@id,'haveDependents')]"));
		String getVal = null;
	    for(WebElement option:radio_options)
	        {
	            if(option.isSelected())
	            {
	              getVal=option.getAttribute("value");
	             break;
	            }
	        }
		return getVal;
	}
	
//**************************************************************************************************************************************************************
public void onEnterRentAggreement(int amount) {
	driver.findElement(By.xpath(monthlyRent_XPATH)).sendKeys(Integer.toString(amount));
}
//**************************************************************************************************************************************************************
public String getRentAggreementTxt() {
	return webActionHelperMethods.getobjectValue(monthlyRent_XPATH);
}
//**************************************************************************************************************************************************************
public void selectBankWith(int row) {
	Select dropdown = new Select(driver.findElement(By.xpath(bankName_XPATH)));
	dropdown.selectByIndex(row);
	
}
//**************************************************************************************************************************************************************
public String getSelectedBankProperty() {
	//List<WebElement> dropdown_options = driver.findElements(By.xpath(bankName_XPATH));
	Select busstype = new Select(driver.findElement(By.xpath(bankName_XPATH)));
    WebElement options=busstype.getFirstSelectedOption();
    String getVal = options.getText();   
	return getVal;
}
//**************************************************************************************************************************************************************
public String getYrswithBranchRadioButtonProperty() {
	List<WebElement> radio_options = driver.findElements(By.xpath(yearsWithBank_XPATH));
	String getVal = null;
    for(WebElement option:radio_options)
        {
            if(option.isSelected())
            {
              getVal=option.getAttribute("value");
             break;
            }
        }
	return getVal;
}

//**************************************************************************************************************************************************************

public void onEnterPPSNo(int amount) {
	driver.findElement(By.xpath(ppsNumber_XPATH)).sendKeys(Integer.toString(amount));
}
//**************************************************************************************************************************************************************
public String getPPSTxt() {
	return webActionHelperMethods.getobjectValue(ppsNumber_XPATH);
}
//**************************************************************************************************************************************************************
public void onEnterPlaceOfBirth(String place) {
	driver.findElement(By.xpath(placeOfBirth_XPATH)).sendKeys(place);
}
//**************************************************************************************************************************************************************
public String getPlaceofBirthValue() {
	return webActionHelperMethods.getobjectValue(placeOfBirth_XPATH);
}

//**************************************************************************************************************************************************************
public String getDateOfBirthTxt() {
	return webActionHelperMethods.getobjectValue(dateOfBirth_XPATH);
}

//**************************************************************************************************************************************************************
public String getPayementFreqRadioButtonProperty() {
	List<WebElement> radio_options = driver.findElements(By.xpath(paymentFrequency_XPATH));
	String getVal = null;
    for(WebElement option:radio_options)
        {
            if(option.isSelected())
            {
              getVal=option.getAttribute("value");
             break;
            }
        }
	return getVal;
}


//**************************************************************************************************************************************************************
public String getIncPayementTxt() {
	return webActionHelperMethods.getobjectValue(incomePayement_XPATH);
}

//**************************************************************************************************************************************************************

}
