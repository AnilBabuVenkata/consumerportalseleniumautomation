package Pages;

import actionHelper.WebActionHelperMethods;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;

public class BasePageClass {
    protected RemoteWebDriver driver;
     WebActionHelperMethods webActionHelperMethods;

    public BasePageClass(RemoteWebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        webActionHelperMethods= new WebActionHelperMethods(driver);
    }

}
