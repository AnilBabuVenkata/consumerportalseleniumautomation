package Pages;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;

import util.ExcelFactory;
import util.RestCalls;
import util.utility;

public class AgriculturePage extends BasePageClass{
	public utility Util=new utility();
	private String uname=Util.randomIdentifier();
	String uniquename,email_id, OTP;

	public AgriculturePage(RemoteWebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	//*************************************************************************************************************************************************
		public static final String country_dropdown = "//select[@id='country']";
		public static final String assetType_dropdown = "//select[@id='assetType']";
		public static final String assetdesc_dropdown = "//input[@id='assetDescription']";
		public static final String assetyear_desc = "//input[@id='assetYear']";
		public static final String deposit_txtfield = "//input[@id='deposit']";
		public static final String financeamt_txtfield = "//input[@id='financeAmount']";
		public static final String amtreq_txtfield = "//input[@id='amountRequested']";
		public static final String bussapplicanttype_dropdown = "//select[@id='applicantType']";
		public static final String legalname_txtfield = "//input[@id='legalName']";
		public static final String cronumber_txtfield = "//input[@id='CRONumber']";
		public static final String busspremiumstatus_dropdown = "//select[@id='businessPremisesStatus']";
		public static final String busspostcode_txtbox = "//input[@id='businessPostcode']";
		public static final String bussstreetname_txtbox = "//input[@id='businessStreetName']";
		public static final String busstown_txtbox = "//input[@id='businessTown']";
		public static final String busscounty_txtbox = "//input[@id='businessCounty']";
		public static final String busstrading_txtbox = "//input[@id='businessYearsTrading']";
		public static final String natofbusiness_txtbox = "//input[@id='natureOfBusiness']";
		public static final String cmppartaddress_radiogroup = "//input[contains(@id,'homeAddressSame')]";
		public static final String progressmotorapp_sidebyarrow = "//a[contains(text(),'Appliant Details')]";
		public static final String persondetails_sidebyarrow = "//a[contains(text(),'Personal Details')]";
		public static final String addressdetails_sidebyarrow = "//a[contains(text(),'Address Details')]";
		public static final String employeedetails_sidebyarrow = "//a[contains(text(),'Employment Details')]";
		public static final String fileupload_sidebyarrow = "//a[contains(text(),'File Upload')]";
		public static final String uploadafile_browse = "//span[text()=' Upload a file ']";
		public static final String draganddrop_upload = "//div[@class='dz-message']//h1";
		
		@FindBy(xpath="//input[contains(@id,'methodOfContact')]")
	    WebElement modeofcontact_radiogroup;
		
		@FindBy(xpath="//select[@id='proposedContactTime']")
		WebElement prefcontacttime_dropdown;
		
		@FindBy(xpath="//select[@id='applicantType']")
		WebElement byssapplicanttype_dropdown;
		
		@FindBy(xpath="//input[@id='legalName']")
		WebElement leganname_txtfield;
		
		@FindBy(xpath="//select[@id='atitle']")
		WebElement title_dropdown;
		
		@FindBy(xpath="//input[@id='afirstName']")
		WebElement firstname_txtfield;
		
		@FindBy(xpath="//input[@id='asurname']")
		WebElement surname_txtfield;
		
		@FindBy(xpath="//input[@id='emailAdd']")
		WebElement emailId_txtfield;
		
		@FindBy(xpath="//input[contains(@id,'homeAddressSame')]")
		WebElement companypartneraddress_radiogroup;
		
		@FindBy(xpath="//input[@id='homeAddressStreet']")
		WebElement streetaddress_txtbox;
		
		@FindBy(xpath="//input[@id='homeAddressTown']")
		WebElement homeaddtown_txtbox;
		
		@FindBy(xpath="//select[@id='homeAddressCounty']")
		WebElement county_dropdown;
		
		@FindBy(xpath="//input[@id='homeAddressPostCode']")
		WebElement homeaddresspostcode_txtbox;
		
		@FindBy(xpath="//input[contains(@id,'residentialStatus')]")
		WebElement residentialstatus_radiogroup;
		
		@FindBy(xpath="//select[@id='yearsAtCurrentAddress']")
		WebElement yrsatcurradd_dropdown;
		
		
		
		
		
		
		
		
		
	//********************************************************************************************************************************************************	
		public void clickOnModeofContact(int i) {
			List<WebElement> radio_options = driver.findElements(By.xpath("//input[contains(@id,'methodOfContact')]"));
			radio_options.get(i).click();
		  	}
	//********************************************************************************************************************************************************	
		public void fillAgriForm() throws Exception {
			ExcelFactory exclfactory = new ExcelFactory();
			exclfactory.ExcelFactory("F:\\FusionSoftTech\\Automation\\TestData\\ConsumerPortal.xls", "Agri");
			webActionHelperMethods.selectdropdown(country_dropdown, exclfactory.excelReadTextCell(1,0).toString()); //Read data from Country Column
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.selectdropdown(assetType_dropdown, exclfactory.excelReadTextCell(1,1).toString()); //Read data from AssetType Column
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,2).toString(), assetdesc_dropdown);  //Read data from Asset Description Column
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,3).toString(), assetyear_desc); //Read Data from Asset Year
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,4).toString(), amtreq_txtfield); //Read Data from Amt Req Column
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,5).toString(), deposit_txtfield); //Read Data from Deposit Column
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			//webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,6).toString(), financeamt_txtfield); //Read Data from Finance Amt
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.clickonradiobutton(CommonPage.hearabout_dropdown, exclfactory.excelReadTextCell(1,7).toString());
			//webActionHelperMethods.selectdropdown(CommonPage.hearabout_dropdown,exclfactory.excelReadTextCell(1,7).toString()); //Read Data from Hear About
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.writeOnEditText(""+uname+"@gmail.com",CommonPage.emailadd_txtbox); //Unique email generator method
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.selectdropdown(CommonPage.title_drpdwn, exclfactory.excelReadTextCell(1,8).toString()); //Read Data from Title
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.writeOnEditText(uname,CommonPage.firstname_txtfield); //Unique Name from MMethod
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,9).toString(),CommonPage.surname_txtfield); //Read Data from SurName
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,10).toString(),CommonPage.mobile_txtfield); //Read Data from Mobile
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.clickonradiobutton(CommonPage.modeofcont_radiogroup,exclfactory.excelReadTextCell(1,11).toString()); //Read Data from modeofcont
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.selectdropdown(CommonPage.propcontacttime_drpdown,exclfactory.excelReadTextCell(1,12).toString()); //Read Data from propcontacttime
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,13).toString(),CommonPage.password_txtbox); //Read Data from Password
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			//String otpurl="http://51.145.252.81/api/user/otp/"+""+uname+"@gmail.com";
			String otpurl="https://my.loanitt.com/api/user/otp/"+""+uname+"@gmail.com";
			//String otpurl="https://api.twilio.com/2010-04-01/Accounts/AC90f8c59d12ac5c803aa5be418ab8e25a/Messages.json?PageSize=1&Page=0";
			RestCalls restcalls=new RestCalls();
			String OTPVal=restcalls.getRequest(otpurl,"","","OTP");
			//OTP=restcalls.getRequest(otpurl,"AC90f8c59d12ac5c803aa5be418ab8e25a","2c4193fba4e25c84c07a41508d894fad","body");
			  System.out.println("---->"+OTPVal);
			Thread.sleep(3000);
			webActionHelperMethods.writeOnEditText(OTPVal,CommonPage.aotp_txtfield);
		//Thread.sleep(10000);
			WebElement element = driver.findElement(By.id("machineOTP"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);

		   // webActionHelperMethods.clickbutton(CommonPage.macotp_button);
			//webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.selectdropdown(bussapplicanttype_dropdown,exclfactory.excelReadTextCell(1,14).toString()); //Read data from BussAppType Column
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,15).toString(),legalname_txtfield ); //Reading Data from Legal Name Column
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,16).toString(),cronumber_txtfield); //Reading from CRONo Column
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.selectdropdown(busspremiumstatus_dropdown ,exclfactory.excelReadTextCell(1,17).toString()); //Reading from BussPremStatus
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,18).toString(),busspostcode_txtbox); //Reading Data from BussPostCode
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,19).toString(),bussstreetname_txtbox); //Reading Data from BussStrtName
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,20).toString(),busstown_txtbox); //Reading Data from BussTown
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,21).toString(),busscounty_txtbox); //Reading Data from County
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,22).toString(),busstrading_txtbox); //Reading Data from BussTrading
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,23).toString(),natofbusiness_txtbox); //Reading Data from NatofBuss
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.selectdropdown(CommonPage.title_drpdwn, exclfactory.excelReadTextCell(1,24).toString()); //Reading Data from Title
		    webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,25).toString(),CommonPage.afirstname_txtfield); //Reading Data from aFirstName
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,26).toString(),CommonPage.asurname_txtfield); //Reading Data from asurname
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,27).toString(),CommonPage.aemailadd_txtbox); //Reading Data from Email
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,28).toString(),CommonPage.dob_txtfield); //Reading Data from aDob
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			/*webActionHelperMethods.writeOnEditText("T",CommonPage.maidenname_txtfield);
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.clickonradiobutton(CommonPage.martstatus_dropdown, "Single");
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.writeOnEditText("0",CommonPage.noofdepend_dropdown);
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.writeOnEditText("Test Street",CommonPage.streetaddress_txtbox);
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.writeOnEditText("Test Town",CommonPage.homeaddtown_txtbox);
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.selectdropdown(CommonPage.county_dropdown, "Ireland");
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.writeOnEditText("9001",CommonPage.homeaddresspostcode_txtbox);
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.clickonradiobutton(CommonPage.residentialstatus_radiogroup, "Home Owner");
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.selectdropdown(CommonPage.yrsatcurradd_dropdown, "2");
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.writeOnEditText("9001",CommonPage.ppsn_txtfield);
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.writeOnEditText("INDIAN",CommonPage.nationality_txtfield);
			webActionHelperMethods.clickbutton(CommonPage.continue_button);
			webActionHelperMethods.writeOnEditText("15000",CommonPage.annincome_txtfield);
			webActionHelperMethods.clickbutton(CommonPage.continue_button);*/
			webActionHelperMethods.clickonradiobutton(cmppartaddress_radiogroup,exclfactory.excelReadTextCell(1,29).toString());  //Reading data from PartAddress
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,5);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,30).toString(),CommonPage.streetaddress_txtbox); //Reading Data from Street Address
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,5);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,31).toString(),CommonPage.homeaddtown_txtbox); //Reading Data from Town
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,5);
			webActionHelperMethods.selectdropdown(CommonPage.county_dropdown,exclfactory.excelReadTextCell(1,32).toString()); //Reading Data from County
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,5);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,33).toString(),CommonPage.homeaddresspostcode_txtbox); //Reading Data fro Post Code
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,5);
			webActionHelperMethods.clickonradiobutton(CommonPage.residentialstatus_radiogroup,exclfactory.excelReadTextCell(1,34).toString()); //Reading Data from Resid Status
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,5);
			webActionHelperMethods.selectdropdown(CommonPage.yrsatcurradd_dropdown,exclfactory.excelReadTextCell(1,35).toString()); //Reading Data from Current Year 
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,5);
			webActionHelperMethods.clickonradiobutton(CommonPage.martstatus_dropdown,exclfactory.excelReadTextCell(1,36).toString()); //Reading Data from Maritial Status
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,5);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,37).toString(),CommonPage.noofdepend_dropdown); //Reading Data from no of dependents
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,5);
			webActionHelperMethods.writeOnEditText(exclfactory.excelReadTextCell(1,38).toString(),CommonPage.ppsn_txtfield); //Reading Data from PPSN No 
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,5);
			webActionHelperMethods.clickonradiobutton(Mortgageswitchpage.jointapp_radiogroup, exclfactory.excelReadTextCell(1,39).toString()); //Reading Data from Joint App
			webActionHelperMethods.ClickButtonAndWait(CommonPage.continue_button,5);
			webActionHelperMethods.ClickButtonAndWait(MotorPage.letsgo_button,10);
			
			exclfactory.finalize();
			
		}
	//********************************************************************************************************************************************************
		
		/*	public void clickOnApplicantDetials() throws Exception {
				Thread.sleep(5000);
				//webActionHelperMethods.ClickButtonAndWait(progressmotorapp_sidebyarrow,10);
				webActionHelperMethods.ClickButtonAndWait(fileupload_sidebyarrow,10);
				webActionHelperMethods.ClickButtonAndWait(uploadafile_browse,10);
				//webActionHelperMethods.clickbutton(applicantdetails_link);
				//webActionHelperMethods.clickbutton(appicantname_drill);
				//webActionHelperMethods.clickbutton(applicantdetails_link);
				Thread.sleep(3000);
				//webActionHelperMethods.ClickButtonAndWait(persondetails_sidebyarrow,10);
			}*/
	//********************************************************************************************************************************************************
			public void clickOnFileUpload() throws Exception {
				webActionHelperMethods.ClickButtonAndWait(fileupload_sidebyarrow,10);	
				webActionHelperMethods.ClickButtonAndWait(uploadafile_browse,10);
				webActionHelperMethods.ClickButtonAndWait(draganddrop_upload,10);
				WebElement addFile = driver.findElement(By.xpath(".//input[@type='file']"));
				String path="C:\\Users\\DELL\\Downloads\\Roses.JPG";
		        StringSelection strSelection = new StringSelection(path);
		        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		        clipboard.setContents(strSelection, null);
		        Robot robot = new Robot();
		        robot.delay(300);
		        robot.keyPress(KeyEvent.VK_ENTER);
		        robot.keyRelease(KeyEvent.VK_ENTER);
		        robot.keyPress(KeyEvent.VK_CONTROL);
		        robot.keyPress(KeyEvent.VK_V);
		        robot.keyRelease(KeyEvent.VK_V);
		        robot.keyRelease(KeyEvent.VK_CONTROL);
		        robot.keyPress(KeyEvent.VK_ENTER);
		        robot.delay(200);
		        robot.keyRelease(KeyEvent.VK_ENTER);
		        
		        Thread.sleep(10000);
		        Assert.assertTrue(driver.findElement(By.xpath("//a[contains(text(),'Roses.jpg')]")).isDisplayed());
				
			}
			
			
			
	//********************************************************************************************************************************************************		
			
			
			
			
			
			
	}
