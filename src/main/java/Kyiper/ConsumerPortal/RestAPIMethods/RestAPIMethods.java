package Kyiper.ConsumerPortal.RestAPIMethods;

import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class RestAPIMethods {
//*****************************************************************************************************************************
	//Get Requests in Rest API
	
	public String getRequests(String Resturl) throws UnirestException {
		String searchQueryApi = Resturl;

		JsonNode body = Unirest.get(searchQueryApi)
		                        .asJson()
		                        .getBody();
		System.out.println(body);         // gives the full json response
		return body.toString();
		
	}
//*****************************************************************************************************************************
	//Post Requests in Rest API
	
	public String postRequests(String Resturl) {
		String postApi = Resturl;
		
		/*Unirest.post(postApi)
        .header("accept", "application/json")
        .header("Content-Type", "application/json")
        .body(template.render(model))
        .asJson();*/
		return Resturl;
		
	}
	
	
	
	
//*****************************************************************************************************************************	
}
