package util;


import io.restassured.RestAssured;	
import io.restassured.config.HttpClientConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.json.JSONException;
import org.testng.Assert;
import org.apache.http.params.CoreConnectionPNames;



import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;

public class RestCalls {

    public static Response postRequest(String url, String paramaters,String uname,String pwd) {
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        //request.header("Content-Type", headerType);
        //request.header("x-resource-service-instance-guid", "fnd");
        
      
        if(!uname.contentEquals("")) {
        request.auth().preemptive().basic(uname,pwd);
        }else if(!uname.contentEquals("uauth")) {
        	request.auth().preemptive().oauth2(pwd);
        }
        
        
        System.out.println("Url : " + url);
        System.out.println("parameters : " + paramaters);
        if (paramaters != "NULL") {
            JSONObject jsonObj = new JSONObject(paramaters);
            System.out.println(jsonObj.toString());

            request.body(jsonObj.toString());
        }
       // restSessiontimeout();             //Timesync
        Response response = null;
        try {
            response = request.post(url);
            // jsonparser(response);
        } catch (Exception e) {
            e.printStackTrace();

        }
        JsonPath jsonPathEvaluator = response.jsonPath();
        // Get Response Body as String

        String bodyStringValue = response.body().asString();


        // int statusCode = response.getStatusCode();
        // System.out.println("Retruned Status Code : " + statusCode);   //paramterize

        // return Integer.toString(statusCode);
        return response;
    }

    //**************************************************************************************************************************************************************************
    public static String getRequest(String url,String uname,String pwd,String attname) throws Exception {

        RequestSpecification httpRequest = RestAssured.given();
        if(!uname.contentEquals("")) {
        httpRequest.auth().preemptive().basic(uname,pwd);
        }
        httpRequest.header("Content-Type", "application/json");
      //  httpRequest.header("x-resource-service-instance-guid", "fnd");
        // Making GET request directly by RequestSpecification.get() method

        restSessiontimeout();             //Timesync
        Response response = null;
        try {
            response = httpRequest.get(url);
            // jsonparser(response);
        }
        // Response response = httpRequest.get(url);
        catch (Exception e) {
            e.printStackTrace();

        }
        //String body = response.getBody().asString();
        JsonPath jsonPathEvaluator = response.jsonPath();
        // Get Response Body as String
        System.out.println("Asserting");
        System.out.println("Actual StatusCode:==>" + response.statusCode());
        System.out.println(parseResponsetemp(response,attname));
        
        //String msg_response=parseResponsetemp(response,attname).split(" ")[3];
        Assert.assertEquals(response.getStatusCode() == 200, true, "Elastic Server Request is successful");
        String respval=parseResponsetemp(response,attname);
        String bodyStringValue = response.body().asString();
        return respval;

    }

    //**************************************************************************************************************************************************************************
    public static String deleteRequest(String url) {
        RequestSpecification httpRequest = RestAssured.given();
        httpRequest.auth().preemptive().basic("anilbabu","Welcome1");
        restSessiontimeout();             //Timesync
        Response response = httpRequest.delete(url);


        int statusCode = response.getStatusCode();
        System.out.println("Retruned Status Code : " + statusCode);

        return Integer.toString(statusCode);
    }

    //**************************************************************************************************************************************************************************
    /*
     * ReadJSONFile() method will read json file and return the node value of the node attribute specified as a parameter
     */
    public static String ReadJSONFile(String nodeValuie, String filePath) throws FileNotFoundException, IOException, ParseException {
        JSONParser parser = new JSONParser();
        Object obj = parser.parse(new FileReader(filePath));
        org.json.simple.JSONObject jsonObject = (org.json.simple.JSONObject) obj;
        String value = (String) jsonObject.get(nodeValuie);
        return value;
    }
//**************************************************************************************************************************************************************************

    public static Response putRequest(String url, String paramaters) {
        RequestSpecification request = RestAssured.given();

        request.auth().preemptive().basic("","");

        request.header("Content-Type", "application/json");
        request.header("x-resource-service-instance-guid", "fnd");

        if (paramaters != "NULL") {
            JSONObject jsonObj = new JSONObject(paramaters);

            request.body(jsonObj.toString());
        }
        restSessiontimeout();             //Timesync
        Response response = request.put(url);

/* int statusCode = response.getStatusCode();
System.out.println("Retruned Status Code : " + statusCode);
Assert.assertEquals(statusCode==200, true , "Request is successful");*/
        //return Integer.toString(statusCode);
        return response;
    }

    //**************************************************************************************************************************************************************************
    public static void restSessiontimeout() {
        RestAssuredConfig config = RestAssured.config()

                .httpClient(HttpClientConfig.httpClientConfig()
                        .setParam(CoreConnectionPNames.CONNECTION_TIMEOUT, 1000)
                        .setParam(CoreConnectionPNames.SO_TIMEOUT, 1000));

    }

    //**************************************************************************************************************************************************************************
    public static String getRequestforstarter(String url) {

        RequestSpecification httpRequest = RestAssured.given();
        httpRequest.auth().preemptive().basic("","");
        httpRequest.header("Content-Type", "application/json");

        // Making GET request directly by RequestSpecification.get() method

        restSessiontimeout();             //Timesync
        Response response = null;
        try {
            response = httpRequest.get(url);
            // jsonparser(response);
        }
        // Response response = httpRequest.get(url);
        catch (Exception e) {
            e.printStackTrace();

        }
        //String body = response.getBody().asString();
        JsonPath jsonPathEvaluator = response.jsonPath();
        // Get Response Body as String
        Assert.assertEquals(response.getStatusCode() == 200, true, "Elastic Server Request is successful");
        System.out.println("Actual StatusCode:==>" + response.statusCode());
        String bodyStringValue = response.body().asString();
        return bodyStringValue;

    }

    //**************************************************************************************************************************************************************************
  
    //**************************************************************************************************************************************************************************
    public static Map<String, String> responseParser(Response responsebody, String ParentField, String... ChildFields) throws Exception {
        Map<String, String> resultantObjects = new HashMap<String, String>();

        JSONParser parse = new JSONParser();
        String resp = responsebody.body().asString();
        System.out.println("------->" + resp);
        //JSONObject jobj = (JSONObject)parse.parse(resp);
        org.json.simple.JSONObject jobj = (org.json.simple.JSONObject) parse.parse(resp);
        System.out.println(jobj.get(ParentField));

// System.out.println("Number Of Child Objects -> "+( jsonarr_1).size());
        //Get data for Results array
        if (ParentField.equals("NULL")) {
            JSONArray jsonarr_1 = (JSONArray) jobj.get("{");
            for (int i = 0; i < jsonarr_1.size(); i++) {

                org.json.simple.JSONObject jsonobj_1 = (org.json.simple.JSONObject) jsonarr_1.get(i);

                for (int argslist = 0; argslist < ChildFields.length; argslist++)
                    resultantObjects.put(ChildFields[argslist] + i, jsonobj_1.get(ChildFields[argslist]).toString());
            }
            System.out.println(jobj.get("{"));
        } else {
            JSONArray jsonarr_1 = (JSONArray) jobj.get(ParentField);
            for (int i = 0; i < jsonarr_1.size(); i++) {

                org.json.simple.JSONObject jsonobj_1 = (org.json.simple.JSONObject) jsonarr_1.get(i);

/*System.out.println(jsonobj_1.get("userName"));
System.out.println(jsonobj_1.get("description"));
System.out.println(jsonobj_1.get("qualifiedBusinessObject"));*/
                for (int argslist = 0; argslist < ChildFields.length; argslist++)
                    resultantObjects.put(ChildFields[argslist] + i, jsonobj_1.get(ChildFields[argslist]).toString());
/* System.out.println(jsonobj_1.get(ChildFields[0]));
System.out.println(jsonobj_1.get(ChildFields[1]));
System.out.println(jsonobj_1.get(ChildFields[2]));*/

            }
        }

        return resultantObjects;
    }

    /*
     * parseJson() will parse the json specified in the "filePath" attribute and returns the result as a "list of map" object
     */
//**************************************************************************************************************************************************************************
    public static String parseResponsetemp(Response responsebody, String attr) throws Exception {
        String resultvalue = null;
        JsonFactory jsonFactory = new JsonFactory();

        JsonParser parser = jsonFactory.createParser(responsebody.body().asString());

        while (!parser.isClosed()) {
            JsonToken token = parser.nextToken();

            if (JsonToken.FIELD_NAME.equals(token)) {
                String fieldName = parser.getCurrentName();
                System.out.println("FieldName  -> " + fieldName);
                token = parser.nextToken();
                if (attr.equals(fieldName) && ((JsonToken.VALUE_STRING.equals(token)) || JsonToken.VALUE_NUMBER_INT.equals(token) || JsonToken.VALUE_NUMBER_FLOAT.equals(token))) {
                    System.out.println("Attribute field Value -> " + parser.getValueAsString());
                    resultvalue = parser.getValueAsString();
                }
            }
        }
        return resultvalue;
    }

    //**************************************************************************************************************************************************************************
    public static Map<String, List<Object>> parseJson(String parentNode, String filePath) throws FileNotFoundException, IOException, ParseException {
        Map<String, List<Object>> requestMapList = new HashMap<String, List<Object>>();
        JSONParser parser = new JSONParser();

        Object parsingObject = parser.parse(new FileReader(filePath));
        org.json.simple.JSONObject jsonObject = (org.json.simple.JSONObject) parsingObject;
        JSONArray parentJsonArray = (JSONArray) jsonObject.get(parentNode);
        System.out.println("Json Array Size -> " + (parentJsonArray).size());

        /*
         * loop through all the child arrays of the parent array passed as an argument to parseJson() method
         */
        for (int loop = 0; loop < parentJsonArray.size(); loop++) {
            List<Object> attributesList = new ArrayList<Object>();
            org.json.simple.JSONObject childArrayObject = (org.json.simple.JSONObject) parentJsonArray.get(loop);
            System.out.println("Child Object of index [" + loop + "] -> " + childArrayObject);
            System.out.println("Requesttype -> " + childArrayObject.get("Requesttype"));
            System.out.println("RequestURL -> " + childArrayObject.get("RequestURL"));
            System.out.println("KeyDescription -> " + childArrayObject.get("KeyDescription"));
            attributesList.add((String) childArrayObject.get("Requesttype"));
            attributesList.add((String) childArrayObject.get("RequestURL"));

            /*
            attributesList.add((String) childArrayObject.get("SQLQuery"));
            attributesList.add((String) childArrayObject.get("CheckColumnName"));
            attributesList.add((String) childArrayObject.get("ExpectedValue"));
            attributesList.add((String) childArrayObject.get("ErrorMessage"));
            attributesList.add((String) childArrayObject.get("SuccessMessage"));
            //attributesList.add((String) childArrayObject.get("RequestURL"));
*/
            if (childArrayObject.containsKey("Payloads")) {
                JSONArray payloadChildArray = (JSONArray) childArrayObject.get("Payloads");
                /*
                 * loop through all the "payloads" child arrays
                 */
                for (int payloadChildLoop = 0; payloadChildLoop < payloadChildArray.size(); payloadChildLoop++) {
                    attributesList.add(payloadChildArray.get(payloadChildLoop));
                    System.out.println("Payload [" + payloadChildLoop + "] -> " + payloadChildArray.get(payloadChildLoop));
                }
            }
/*JSONArray payloadChildArray = (JSONArray) childArrayObject.get("Payloads");

 * loop through all the "payloads" child arrays
 

if(payloadChildArray!=null)
{
for(int payloadChildLoop=0;payloadChildLoop < payloadChildArray.size();  payloadChildLoop++) {
attributesList.add(payloadChildArray.get(payloadChildLoop));
System.out.println("Payload ["+payloadChildLoop+"] -> "+payloadChildArray.get(payloadChildLoop));
}
}
*/
            requestMapList.put((String) childArrayObject.get("KeyDescription"), attributesList);
            // attributesList.clear();
        }
        return requestMapList;
    }//End Of parseJson()

  

}
