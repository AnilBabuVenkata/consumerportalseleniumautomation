package util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class ExcelFactory {

	private InputStream iFile;
	private OutputStream oFile;
	private Workbook oBook;
	private Sheet oSheet;
	private Row oRow;
	private Cell oCell;
	private String fileName;
	
	
	public ExcelFactory() {
		
	}
	
	public void ExcelFactory(String fileName, String sheetName) {
		try {
			iFile = new FileInputStream(fileName);
			oBook = WorkbookFactory.create(iFile);
			oSheet = oBook.getSheet(sheetName);
			this.fileName = fileName;
		}
		catch(Exception e) {
			
		}
	}
	
	public void finalize() {
		try {
			oFile.close();
			iFile.close();
		}
		catch (Exception e) {
			
		}
	}
	
	public String excelReadNumericCell(int rowIndex, int colIndex) {
			oRow = oSheet.getRow(rowIndex);
			return String.valueOf(oRow.getCell(colIndex).getNumericCellValue());
	}
	
	public String excelReadTextCell(int rowIndex, int colIndex) {
		oRow = oSheet.getRow(rowIndex);
		return oRow.getCell(colIndex).getStringCellValue();
	}

	public void excelWriteCell(int rowIndex, int colIndex, String valueToWrite) {
			oRow = oSheet.getRow(rowIndex);
			oCell = oRow.getCell(colIndex);
			if (oCell == null) {
				oRow.createCell(colIndex);
				oCell = oRow.getCell(colIndex);
			}
			oCell.setCellValue(valueToWrite);
			try {
				oFile = new FileOutputStream(this.fileName);
				oBook.write(oFile);
			}
			catch(Exception e) {
				
			}
	}
	
	public int getRowcount() {
		return oSheet.getLastRowNum();
	}

}
