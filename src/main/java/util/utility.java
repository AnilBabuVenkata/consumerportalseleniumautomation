package util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.Properties;
import java.util.Random;
import java.util.Set;

public class utility {

    static Properties properties;
    static InputStream input;
    final String lexicon = "ABCDEFGHIJKLMNOPQRSTUVWXYZ12345674890";

    final java.util.Random rand = new java.util.Random();

    // consider using a Map<String,Boolean> to say whether the identifier is being used or not 
    final Set<String> identifiers = new HashSet<String>();

    public static Properties loadProperties(String path) {
        try {
            input = new FileInputStream(path);
            properties = new Properties();
            properties.load(input);
            return properties;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }
    
  
    	
    
   
    public String randomIdentifier() {
        // lower limit for LowerCase Letters
        int lowerLimit = 97;
  
        // lower limit for LowerCase Letters
        int upperLimit = 122;
  
        Random random = new Random();
  
        // Create a StringBuffer to store the result
        StringBuffer r = new StringBuffer(5);
  
        for (int i = 0; i < 5; i++) {
  
            // take a random value between 97 and 122
            int nextRandomChar = lowerLimit
                                 + (int)(random.nextFloat()
                                         * (upperLimit - lowerLimit + 1));
  
            // append a character at the end of bs
            r.append((char)nextRandomChar);
        }
  
        // return the resultant string
        return r.toString();
    }
  

        
    
}
