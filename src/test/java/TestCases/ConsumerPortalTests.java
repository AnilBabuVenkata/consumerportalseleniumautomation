package TestCases;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.Test;

import Pages.MyApplicationsPage;
import Pages.SignUp;
import io.restassured.response.Response;

public class ConsumerPortalTests extends BaseTest {
     Map<String,List<Object>> postObject;
     Map<String,String> responseObject;
     String response,emailId;
     String OTP,accesstoken,guiid;
	
//****************************************************************************************************************************
	@Test(description="For Signup",priority=1,enabled=false)
	public void signUpTestCase() {
		driver.get(URL);
		driver.manage().window().maximize();
		signinPage.clickOnSignUp();
		Assert.assertEquals(signupPage.getHeader(),"Sign Up");
	}
//****************************************************************************************************************************	
   @Test(description="Enter Details in Signup",priority=2,enabled=false)
   public void enterSignupDetails() {
	   signupPage.Signup(); 
	   emailId=signupPage.getEmailIdValue();
	   
   }



//****************************************************************************************************************************
  @Test(description="Get OTP from Twills site",priority=3,enabled=false)
  public void getOTPfromMobile() throws Exception {
	  //Getting OTP
	  OTP=restCalls.getRequest(OTPURL,"AC90f8c59d12ac5c803aa5be418ab8e25a","2c4193fba4e25c84c07a41508d894fad","body");
	  System.out.println("---->"+OTP);
  }
   
 //****************************************************************************************************************************  
  @Test(description="Login with OTP",priority=4,enabled=false)
  public void signInwithOTP() {
	  signinPage.signInWithOTP(OTP);
  }
  
//**************************************************************************************************************************** 
  @Test(description="Get Access Key",priority=5,enabled=false)
  public void getAccessKeybasedOnOTP() throws Exception {
	  Response accessresponse;
	  postObject=restCalls.parseJson("MotorApplication",System.getProperty("user.dir")+"/src/main/java/ConfigurationResources/validateotp.json");
	  String validateOTPURL =QAURL+postObject.get("ValidateOTP").get(1).toString();
	  Object param1=postObject.get("ValidateOTP").get(2);
	  String parameters=param1.toString();
	  parameters=parameters.replace("<emailid>",emailId);
	  parameters=parameters.replace("<mobileotp>",OTP);
	  accessresponse=restCalls.postRequest(validateOTPURL,parameters,"","");
	  System.out.println(accessresponse.getStatusCode());
	  //Getting access Token
	  accesstoken=restCalls.parseResponsetemp(accessresponse, "accessToken");
  }
  
  
//****************************************************************************************************************************
  @Test(description="Create Consumer Application",priority=6,enabled=false)
  public void createConsumerApplication() throws FileNotFoundException, IOException, ParseException {
	  Response accessresponse;
	  postObject=restCalls.parseJson("MotorApplication",System.getProperty("user.dir")+"/src/main/java/ConfigurationResources/motorapp.json");
	  String stageURL =STAGEURL+postObject.get("NewMotorApp").get(1).toString();
	  Object param1=postObject.get("NewMotorApp").get(2);
	  accessresponse=restCalls.postRequest(stageURL,param1.toString(),"","");
	  System.out.println(accessresponse.getStatusCode());
	  System.out.println(accessresponse.getBody().asString());
  }
  
  
//**************************************************************************************************************************** 

  @Test(description="Bind Current User with GUI ID",priority=7,enabled=false)
  public void bindingGUIIdtoCurrentUser() throws FileNotFoundException, IOException, ParseException {
	  Response guidmapresponse;
	  String stageURL =QAURL+"/bindcurrentuser/"+guiid;
	  guidmapresponse=restCalls.postRequest(stageURL,"NULL","uauth",accesstoken);
	  System.out.println(guidmapresponse.getStatusCode());
	  System.out.println(guidmapresponse.getBody().asString());
  }

//***********************************************************************************************************************************************************
  @Test(description="Create Mortgage Application",priority=8,enabled=false)
  public void createMortgageApplication() throws Exception {
	  Response accessmortresponse;
	  postObject=restCalls.parseJson("MortgageApplication",System.getProperty("user.dir")+"/src/main/java/ConfigurationResources/mortgage.json");
	  String stageURL =STAGEURL+postObject.get("NewMortgageApp").get(1).toString();
	  Object param1=postObject.get("NewMortgageApp").get(2);
	  accessmortresponse=restCalls.postRequest(stageURL,postObject.get("NewMortgageApp").get(2).toString(),"","");
	  System.out.println(accessmortresponse.getStatusCode());
	  System.out.println(accessmortresponse.getBody().asString());
	  guiid=restCalls.parseResponsetemp(accessmortresponse, "guid");
	  
  }
//****************************************************************************************************************************
  @Test(description="Click on MyApplications",priority=9,enabled=true)
  public void clickonMyApplications() {
	  driver.get(URL);
	  driver.manage().window().maximize();
	  signinPage.signIn("anilbabu.venkata@fusionsofttech.co.in");
	 // signinPage.signInWithOTP(OTP);
	  dashboardPage.clickOnMyApptab();
	  myapplicationsPage.onClick();
	  myapplicationsPage.clickOnAppliant();
	  }
  
 //****************************************************************************************************************************
  @Test(description="Validation My Applicant Details",priority=10,enabled=true)
  public void validateMyApplicantDetails() {
	  Assert.assertEquals(myapplicationsPage.getGenderTxt(), "Male");
	  Assert.assertEquals(myapplicationsPage.getDOBTxt(), "September 26, 2021");
	  Assert.assertEquals(myapplicationsPage.getMaritalStatusTxt(), "Single");
	  Assert.assertEquals(myapplicationsPage.getMobileTxt(), "N/A");
	  Assert.assertEquals(myapplicationsPage.getEmailTxt(), "N/A");
	  Assert.assertEquals(myapplicationsPage.getPlaceofBirthTxt(), "N/A");
	  myapplicationsPage.clickOnNextBtn();
	  myapplicationsPage.clickOnPersonalDetailsBtn();
	  myapplicationsPage.getRadioButtonProperty();
	  Assert.assertEquals(myapplicationsPage.getRadioButtonProperty(), "Single");
	  myapplicationsPage.clickOnUpdateBtn();
	  Assert.assertEquals(myapplicationsPage.getDependentsRadioButtonProperty(),"no");
	  myapplicationsPage.clickOnUpdateBtn();
	  Assert.assertEquals(myapplicationsPage.getRentAggreementTxt(),"2000");
	  myapplicationsPage.clickOnUpdateBtn();
	  Assert.assertEquals(myapplicationsPage.getSelectedBankProperty(),"AIB");
	  myapplicationsPage.clickOnUpdateBtn();
	  Assert.assertEquals(myapplicationsPage.getYrswithBranchRadioButtonProperty(),"3");
	  myapplicationsPage.clickOnUpdateBtn();
	  Assert.assertEquals(myapplicationsPage.getPPSTxt(),"100");
	  myapplicationsPage.clickOnUpdateBtn();
	  Assert.assertEquals(myapplicationsPage.getPlaceofBirthValue(),"India");
	  myapplicationsPage.clickOnUpdateBtn();
	  Assert.assertEquals(myapplicationsPage.getDateOfBirthTxt(),"09/26/2021");
	  myapplicationsPage.clickOnUpdateBtn();
	  Assert.assertEquals(myapplicationsPage.getPayementFreqRadioButtonProperty(),"Monthly");
	  myapplicationsPage.clickOnUpdateBtn();
	  Assert.assertEquals(myapplicationsPage.getIncPayementTxt(),"50");
	  myapplicationsPage.clickOnUpdateBtn();
	  
	  
	  
  }
 //**************************************************************************************************************************** **************************
  
  
//********************************************************************************************************************************************************* 
}
