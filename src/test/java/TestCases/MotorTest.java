package TestCases;

import java.util.concurrent.TimeUnit;

import org.testng.Assert;
import org.testng.annotations.Test;

import util.ExcelFactory;

public class MotorTest extends BaseTest {
	public ExcelFactory exclfactory;
	
	//*********************************************************************************************************************************************************
	   @Test(priority=1)
	   public void testcase01() throws Exception {
		   driver.get(MOTORURL);
			//driver.manage().window().maximize();
			//driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
			motorPage.fillMotorApplicationForm(driver);
	   }
//*********************************************************************************************************************************************************
	   @Test(priority=2)
	   public void testcase02() throws Exception {
		   motorPage.clickOnApplicantDetials(driver);
	   }
//*********************************************************************************************************************************************************
	   @Test(priority=3)
	   public void testcase03() throws Exception {
		   Thread.sleep(5000);
		   exclfactory = new ExcelFactory();
		   exclfactory.ExcelFactory("F:\\FusionSoftTech\\Automation\\TestData\\ConsumerPortal.xls", "Motor");
		   Assert.assertEquals(motorPage.getMartialStatus(),exclfactory.excelReadTextCell(1,15).toString());
		   motorPage.clickOnUpdateButton();
		   Thread.sleep(2000);
		   Assert.assertEquals(motorPage.getdependentsListValue(),exclfactory.excelReadTextCell(1,16).toString());
		   motorPage.clickOnUpdateButton();
		   Assert.assertEquals(motorPage.getMortageRentvalue(),exclfactory.excelReadTextCell(1,23).toString());
		   motorPage.clickOnUpdateButton();
		   Assert.assertEquals(motorPage.getBankvalue(),exclfactory.excelReadTextCell(1,24).toString());
		   motorPage.clickOnUpdateButton();
		     Assert.assertEquals(motorPage.getYearswithBankvalue(exclfactory.excelReadTextCell(1,25).toString()),exclfactory.excelReadTextCell(1,25).toString());
		  // Assert.assertEquals(motorPage.getYearswithBankvalue(),2);
		   motorPage.clickOnUpdateButton();
		   Assert.assertEquals(motorPage.getPPSValue(),exclfactory.excelReadTextCell(1,26).toString());
		   motorPage.clickOnUpdateButton();
		   Thread.sleep(2000);
		   Assert.assertEquals(motorPage.getPlaceofBirthValue(),exclfactory.excelReadTextCell(1,27).toString());
		   motorPage.clickOnUpdateButton();
		   Assert.assertEquals(motorPage.getDateofBirthValue(),exclfactory.excelReadTextCell(1,28).toString());
		   motorPage.clickOnUpdateButton();
		   Assert.assertEquals(motorPage.getPaymentFreqValue(exclfactory.excelReadTextCell(1,35).toString()),exclfactory.excelReadTextCell(1,35).toString());
		   motorPage.clickOnUpdateButton();
		   Assert.assertEquals(motorPage.getIncomePayValue(),exclfactory.excelReadTextCell(1,36).toString());
		   motorPage.clickOnUpdateButton();
		   Thread.sleep(2000);
		   motorPage.clickOnNextButton();
		
	   }
	   
//*********************************************************************************************************************************************************
	   
	   @Test(priority=4)
	   public void testcase04() throws Exception {
		   Thread.sleep(5000);
		   motorPage.clickOnAddressDetails();
		   Assert.assertEquals(motorPage.getStreetAdressValue(),exclfactory.excelReadTextCell(1,17).toString());
		   motorPage.clickOnUpdateButton();
		   Assert.assertEquals(motorPage.getHomeTownValue(),exclfactory.excelReadTextCell(1,18).toString());
		   motorPage.clickOnUpdateButton();
		   Assert.assertEquals(motorPage.getCountryDrpDownValue(),exclfactory.excelReadTextCell(1,19).toString());
		   motorPage.clickOnUpdateButton();
		   Assert.assertEquals(motorPage.getHomeAddressPostCodeValue(),exclfactory.excelReadTextCell(1,20).toString());
		   motorPage.clickOnUpdateButton();
		   Assert.assertEquals(motorPage.getResidentialStatusValue(exclfactory.excelReadTextCell(1,21).toString()),exclfactory.excelReadTextCell(1,21).toString());
		   motorPage.clickOnUpdateButton();
		   Assert.assertEquals(motorPage.getYrsatCurrAddValue(exclfactory.excelReadTextCell(1,22).toString()),exclfactory.excelReadTextCell(1,22).toString());
		   motorPage.clickOnUpdateButton();
		   Thread.sleep(2000);
		   motorPage.clickOnNextButton();
		   
	   }
//*********************************************************************************************************************************************************	   
	   @Test(priority=5)
	   public void testcase05() throws Exception{
		   Thread.sleep(5000);
		   motorPage.clickOnEmpDetailsSection();
		   Assert.assertEquals(motorPage.getEmpNameValue(),exclfactory.excelReadTextCell(1,29).toString());
		   motorPage.clickOnUpdateButton();
		   Assert.assertEquals(motorPage.getEmpStreetValue(),exclfactory.excelReadTextCell(1,30).toString());
		   motorPage.clickOnUpdateButton();
		   Assert.assertEquals(motorPage.getEmpTownValue(),exclfactory.excelReadTextCell(1,31).toString());
		   motorPage.clickOnUpdateButton();
		   Assert.assertEquals(motorPage.getEmpCountryDrpDownValue(),exclfactory.excelReadTextCell(1,32).toString());
		   motorPage.clickOnUpdateButton();
		   Assert.assertEquals(motorPage.getEmpStatusValue(exclfactory.excelReadTextCell(1,33).toString()),exclfactory.excelReadTextCell(1,33).toString());
		   motorPage.clickOnUpdateButton();
		   Assert.assertEquals(motorPage.getYrswithEmpValue(exclfactory.excelReadTextCell(1,34).toString()),exclfactory.excelReadTextCell(1,34).toString());
		   motorPage.clickOnUpdateButton();
		   Thread.sleep(2000);
		   motorPage.clickOnUpdateButton();
		   
		   
		   exclfactory.finalize();
	   }
//*********************************************************************************************************************************************************

}
