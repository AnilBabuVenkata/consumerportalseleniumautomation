package TestCases;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import Pages.AgriculturePage;
import Pages.CommonPage;
import Pages.DashboardPage;
import Pages.MortgagePage;
import Pages.Mortgageswitchpage;
import Pages.MotorPage;
import Pages.MyApplicationsPage;
import Pages.SignInPage;
import Pages.SignUp;
import actionHelper.WebActionHelperMethods;
import driverManager.DriverManagerType;
import driverManager.WebDrivers;
import util.ExcelFactory;
import util.RestCalls;
import util.supportBaseTest;
import util.utility;

public class BaseTest{

	    protected SignInPage signinPage;
	    protected SignUp signupPage;
	    protected RestCalls restCalls;
	    protected MyApplicationsPage myapplicationsPage;
	    protected DashboardPage dashboardPage;
	    protected Mortgageswitchpage mortgageswitchPage;
	    protected MortgagePage mortgagePage;
	    protected CommonPage commonPage;
	    protected AgriculturePage agriculturePage;
	    protected MotorPage motorPage;
	    
	    RemoteWebDriver driver;
	    Properties properties;
	    //ExcelFactory excelFactory;
	    static String URL;
	    static String OTPURL;
	    static String QAURL,STAGEURL,AGRIURL,MOTORURL,MORTGAGRURL,MORTGSWITCHURL;
	    String username = "keithdoyle";
		String accessKey = "kgaUsUTrFR2bRoqYUaLfSJHs1zACPdjXHju5hilAYZY8AQqaQh";
		
//*****************************************************************************************************************************************
	 
		
		
		
		
		@Parameters({ "platformName","deviceName","platformVersion", "build","name"})
		@BeforeTest
	    public void initTest(String platformName, String deviceName,String platformVersion, String build, String name) {
	        String propertyPath = System.getProperty("user.dir") + "//src//main//resources//Env.properties";
	        //	  driver = WebDrivers.getDriver(DriverManagerType.CHROME);
	             properties = utility.loadProperties(propertyPath);
	              
	              URL = properties.getProperty("URL");
	              OTPURL = properties.getProperty("OTPURL");
	            QAURL =properties.getProperty("QAURL");
	            STAGEURL =properties.getProperty("STAGEURL");
	            AGRIURL =properties.getProperty("AGRIURL");
	            MOTORURL =properties.getProperty("MOTORURL");
	            MORTGAGRURL =properties.getProperty("MORTGAGRURL");
	            MORTGSWITCHURL =properties.getProperty("MORTGSWITCHURL");
	            
	      
	            DesiredCapabilities capabilities = new DesiredCapabilities();
	            capabilities.setCapability("platformName",platformName);
	        	capabilities.setCapability("deviceName", deviceName);
	        	capabilities.setCapability("platformVersion",platformVersion);
	        	capabilities.setCapability("build", build);
	            capabilities.setCapability("name",name);
	           // capabilities.setCapability("appiumVersion","1.19.1");
	            capabilities.setCapability("tunnel",true);
	            capabilities.setCapability("w3c", true);
	            //capabilities.setCapability("network", true); // To enable network logs
	           // capabilities.setCapability("visual", true); // To enable step by step screenshot
	            capabilities.setCapability("video", true); // To enable video recording
	       //     capabilities.setCapability("console", true); // To capture console logs
	           // capabilities.setCapability("unicodeKeyboard", true); // To capture console logs
	     //      capabilities.setCapability("resetKeyboard", true); // To capture console logs
	        
	            try {       
	    			driver= new RemoteWebDriver(new URL("https://"+username+":"+accessKey+"@hub.lambdatest.com/wd/hub"), capabilities);            
	            } catch (MalformedURLException e) {
	                System.out.println("Invalid grid URL");
	            }
	        
	            signinPage = new SignInPage(driver);
	            signupPage = new SignUp(driver);
	            myapplicationsPage = new MyApplicationsPage(driver);
	            dashboardPage = new DashboardPage(driver);
	            commonPage = new CommonPage(driver);
	            mortgageswitchPage = new Mortgageswitchpage(driver);
	            mortgagePage = new MortgagePage(driver);
	            agriculturePage = new AgriculturePage(driver);
	            motorPage = new MotorPage(driver);
	           // excelFactory= new ExcelFactory();
	            restCalls = new RestCalls();  
	    }
 //*****************************************************************************************************************************************
	    @AfterTest
	    public void driverexecution() {
	       driver.quit();
	    }
	    
	    
 //*****************************************************************************************************************************************	    
	}