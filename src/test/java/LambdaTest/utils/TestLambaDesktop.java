package LambdaTest.utils;
//This is maven project



import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;



public class TestLambaDesktop{

	public RemoteWebDriver driver = null;
   String username = "keithdoyle";
	String accessKey = "kgaUsUTrFR2bRoqYUaLfSJHs1zACPdjXHju5hilAYZY8AQqaQh";
	
	@Parameters({ "platformName","browserName","version", "build","name"})
	@BeforeTest
    public void setUp(String platformName, String browserName,String version, String build, String name) throws Exception {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformName", platformName);
    	capabilities.setCapability("browserName", browserName);
    	capabilities.setCapability("version",version);
    	capabilities.setCapability("build", build);
        capabilities.setCapability("name", name);
      
        //capabilities.setCapability("appiumVersion",appiumVersion);
        
        capabilities.setCapability("network", false); // To enable network logs
        capabilities.setCapability("visual", false); // To enable step by step screenshot
        capabilities.setCapability("video", true); // To enable video recording
        capabilities.setCapability("console", false); // To capture console logs
    
        try {       
			driver= new RemoteWebDriver(new URL("https://"+username+":"+accessKey+"@hub.lambdatest.com/wd/hub"), capabilities);            
        } catch (MalformedURLException e) {
            System.out.println("Invalid grid URL");
        }
    }


}
